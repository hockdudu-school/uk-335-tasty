package ch.hockdudu.tasty.connection.model;

final public class RecipeDetailed {
    public int id;
    public String name;
    public String description;
    public int time;
    public Ingredient[] ingredients;
    public Preparation[] preparations;
    public int view_count;
    public Image[] images;
}
