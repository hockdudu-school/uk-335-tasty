package ch.hockdudu.tasty.connection.model;

final public class Ingredient {
    public String ingredient;
    public String quantity;
}
