package ch.hockdudu.tasty.connection;

import android.support.annotation.Nullable;

import java.util.List;

import ch.hockdudu.tasty.connection.adapter.qualifiers.RecipeEditQualifier;
import ch.hockdudu.tasty.connection.adapter.qualifiers.RecipeListQualifier;
import ch.hockdudu.tasty.connection.adapter.qualifiers.SimpleRecipeQualifier;
import ch.hockdudu.tasty.connection.model.Status;
import ch.hockdudu.tasty.connection.model.Token;
import ch.hockdudu.tasty.model.Recipe;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @FormUrlEncoded
    @POST("/auth/register")
    Call<Token> register(@Field("username") String username, @Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("/auth/token")
    Call<Token> login(@Field("username") String username, @Field("password") String password);

    @GET("/recipes/featured")
    @RecipeListQualifier
    @SimpleRecipeQualifier
    Call<List<Recipe>> featuredRecipes(@Header("Authorization") String token);

    @GET("/recipes/my_recipes")
    @RecipeListQualifier
    @SimpleRecipeQualifier
    Call<List<Recipe>> myRecipes(@Header("Authorization") String token);

    @GET("/search")
    @RecipeListQualifier
    @SimpleRecipeQualifier
    Call<List<Recipe>> searchRecipes(@Header("Authorization") String token, @Query("q") String query);

    @GET("/recipes/recipe/{id}")
    Call<Recipe> detailedRecipe(@Header("Authorization") String token, @Path("id") int id);

    @Multipart
    @POST("/recipes/create")
    Call<Recipe> createRecipe(@Header("Authorization") String token, @Part("data") @RecipeEditQualifier Recipe recipe, @Part List<MultipartBody.Part> image);

    @Multipart
    @POST("/recipes/edit/{id}")
    Call<Recipe> editRecipe(@Header("Authorization") String token, @Path("id") int id, @Part("data") @RecipeEditQualifier Recipe recipe, @Part List<MultipartBody.Part> image);

    @DELETE("/recipes/delete/{id}")
    Call<Status> deleteRecipe(@Header("Authorization") String token, @Path("id") int id);

    @FormUrlEncoded
    @POST("/user/edit")
    Call<Status> editUser(@Header("Authorization") String token, @Field("email") @Nullable String email, @Field("password") @Nullable String password);

    @DELETE("/user/delete")
    Call<Status> deleteUser(@Header("Authorization") String token);
}
