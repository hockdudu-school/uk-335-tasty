package ch.hockdudu.tasty.connection.model;

final public class RecipeEdit {
    public String name;
    public String description;
    public int time;
    public Ingredient[] ingredients;
    public Preparation[] preparations;
    public String[] deleted_images;
}
