package ch.hockdudu.tasty.connection.model;

import java.util.List;

final public class RecipeSimple {
    public int id;
    public String name;
    public String description;
    public List<Image> images;
    public int view_count;
}
