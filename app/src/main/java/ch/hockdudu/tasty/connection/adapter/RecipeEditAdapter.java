package ch.hockdudu.tasty.connection.adapter;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import ch.hockdudu.tasty.connection.adapter.qualifiers.RecipeEditQualifier;
import ch.hockdudu.tasty.connection.model.Ingredient;
import ch.hockdudu.tasty.connection.model.Preparation;
import ch.hockdudu.tasty.connection.model.RecipeEdit;
import ch.hockdudu.tasty.model.Recipe;

public class RecipeEditAdapter {

    @FromJson
    @RecipeEditQualifier
    Recipe fromJson(RecipeEdit recipeEdit) {
        throw new RuntimeException("Not implemented");
    }


    @ToJson
    RecipeEdit toJson(@RecipeEditQualifier Recipe recipe) {
        RecipeEdit recipeEdit = new RecipeEdit();

        recipeEdit.name = recipe.getName();
        recipeEdit.description = recipe.getDescription();
        recipeEdit.time = recipe.getPreparationTime();

        recipeEdit.ingredients = new Ingredient[recipe.getIngredients().size()];
        for (int i = 0; i < recipe.getIngredients().size(); i++) {
            Ingredient ingredient = new Ingredient();
            ingredient.ingredient = recipe.getIngredients().get(i).getName();
            ingredient.quantity = recipe.getIngredients().get(i).getQuantity();

            recipeEdit.ingredients[i] = ingredient;
        }

        recipeEdit.preparations = new Preparation[recipe.getPreparations().size()];
        for (int i = 0; i < recipe.getPreparations().size(); i++) {
            Preparation preparation = new Preparation();
            preparation.preparation = recipe.getPreparations().get(i).getDescription();

            recipeEdit.preparations[i] = preparation;
        }

        if (recipe.getImageUrl() != null) {
            recipeEdit.deleted_images = new String[1];
            String[] imageMatches = recipe.getImageUrl().split("/");
            String imageId = imageMatches[imageMatches.length - 1];
            recipeEdit.deleted_images[0] = imageId;
        }

        return recipeEdit;
    }
}
