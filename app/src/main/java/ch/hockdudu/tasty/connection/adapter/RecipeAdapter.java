package ch.hockdudu.tasty.connection.adapter;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import ch.hockdudu.tasty.connection.model.RecipeDetailed;
import ch.hockdudu.tasty.model.Ingredient;
import ch.hockdudu.tasty.model.Preparation;
import ch.hockdudu.tasty.model.Recipe;

public class RecipeAdapter {

    @FromJson
    public Recipe fromJson(RecipeDetailed recipeDetailed) {
        Recipe recipe = new Recipe();

        recipe.setId(recipeDetailed.id);
        recipe.setName(recipeDetailed.name);
        recipe.setDescription(recipeDetailed.description);
        recipe.setPreparationTime(recipeDetailed.time);

        recipe.getIngredients().ensureCapacity(recipeDetailed.ingredients.length);
        for (int i = 0; i < recipeDetailed.ingredients.length; i++) {
            Ingredient ingredient = new Ingredient();
            ingredient.setName(recipeDetailed.ingredients[i].ingredient);
            ingredient.setQuantity(recipeDetailed.ingredients[i].quantity);

            recipe.getIngredients().add(i, ingredient);
        }

        recipe.getPreparations().ensureCapacity(recipeDetailed.preparations.length);
        for (int i = 0; i < recipeDetailed.preparations.length; i++) {
            Preparation preparation = new Preparation();
            preparation.setDescription(recipeDetailed.preparations[i].preparation);

            recipe.getPreparations().add(i, preparation);
        }

        if (recipeDetailed.images.length > 0) {
            recipe.setImageUrl(recipeDetailed.images[0].url);
        }

        return recipe;
    }

    @ToJson
    public RecipeDetailed toJson(Recipe recipe) {
        throw new RuntimeException("Not implemented");
    }

}
