package ch.hockdudu.tasty.connection.model;

final public class RecipesList {
    public int count;
    public RecipeSimple[] recipes;
}
