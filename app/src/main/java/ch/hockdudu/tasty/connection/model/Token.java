package ch.hockdudu.tasty.connection.model;

import android.support.annotation.NonNull;

final public class Token {
    @NonNull
    public String message = "";

    @NonNull
    public String token = "";
}
