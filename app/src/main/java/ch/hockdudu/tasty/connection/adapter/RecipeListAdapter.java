package ch.hockdudu.tasty.connection.adapter;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.ToJson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ch.hockdudu.tasty.connection.adapter.qualifiers.RecipeListQualifier;
import ch.hockdudu.tasty.connection.adapter.qualifiers.SimpleRecipeQualifier;
import ch.hockdudu.tasty.connection.model.RecipesList;
import ch.hockdudu.tasty.connection.model.RecipeSimple;
import ch.hockdudu.tasty.model.Recipe;

public class RecipeListAdapter {

    @FromJson
    public List<RecipeSimple> recipesListFromJson(RecipesList recipesList) {
        return Arrays.asList(recipesList.recipes);
    }

    @ToJson
    public RecipesList recipesListToJson(List<RecipeSimple> recipes) {
        RecipesList recipesList = new RecipesList();
        recipesList.count = recipes.size();
        recipesList.recipes = recipes.toArray(new RecipeSimple[0]);
        return recipesList;
    }

    @FromJson
    @RecipeListQualifier
    @SimpleRecipeQualifier
    public List<Recipe> simpleRecipeFromJson(List<RecipeSimple> recipeSimples) {
        List<Recipe> recipes = new ArrayList<>(recipeSimples.size());

        for (int i = 0; i < recipeSimples.size(); i++) {
            Recipe recipe = new Recipe();
            RecipeSimple recipeSimple = recipeSimples.get(i);

            recipe.setId(recipeSimple.id);
            recipe.setName(recipeSimple.name);
            recipe.setDescription(recipeSimple.description);

            if (recipeSimple.images.size() > 0) {
                recipe.setImageUrl(recipeSimple.images.get(0).url);
            }

            recipes.add(i, recipe);
        }

        return recipes;
    }

    @ToJson
    public List<RecipeSimple> simpleRecipeToJson(@RecipeListQualifier @SimpleRecipeQualifier List<Recipe> recipes) {
        throw new RuntimeException("Not implemented");
    }
}
