package ch.hockdudu.tasty.connection;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

import com.squareup.moshi.Moshi;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import ch.hockdudu.tasty.connection.adapter.RecipeAdapter;
import ch.hockdudu.tasty.connection.adapter.RecipeEditAdapter;
import ch.hockdudu.tasty.connection.adapter.RecipeListAdapter;
import ch.hockdudu.tasty.connection.model.Status;
import ch.hockdudu.tasty.connection.model.Token;
import ch.hockdudu.tasty.model.Recipe;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class Connection {
    private static Connection instance;
    private static String token;

    private ApiService apiService;

    private Connection() {
        // Oh boy, I love this name
        Moshi moshi = new Moshi.Builder()
                .add(new RecipeListAdapter())
                .add(new RecipeEditAdapter())
                .add(new RecipeAdapter())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://uk-335-server.hockdudu.ch/")
                .addConverterFactory(MoshiConverterFactory.create(moshi)) // Hell yeah
                .build();

        this.apiService = retrofit.create(ApiService.class);
    }

    public void login(String username, String password, Callback<Token> callback) {
        this.apiService.login(username, password).enqueue(callback);
    }

    public void signup(String username, String email, String password, Callback<Token> callback) {
        this.apiService.register(username, email, password).enqueue(callback);

    }

    public void featuredRecipes(Callback<List<Recipe>> callback) {
        this.apiService.featuredRecipes(token).enqueue(callback);
    }

    public void myRecipes(Callback<List<Recipe>> callback) {
        this.apiService.myRecipes(token).enqueue(callback);
    }

    public void searchRecipes(String query, Callback<List<Recipe>> callback) {
        this.apiService.searchRecipes(token, query).enqueue(callback);
    }

    public void createRecipe(Recipe recipe, Callback<Recipe> callback) {
        List<MultipartBody.Part> parts = new ArrayList<>(1);

        if (recipe.getRecipeImage() != null) {
            parts.add(getPartFromBitmap(recipe.getRecipeImage()));
        }

        this.apiService.createRecipe(token, recipe, parts).enqueue(callback);
    }

    public void editRecipe(Recipe recipe, int recipeId, Callback<Recipe> callback) {
        List<MultipartBody.Part> parts = new ArrayList<>(1);

        if (recipe.getRecipeImage() != null) {
            parts.add(getPartFromBitmap(recipe.getRecipeImage()));
        }

        this.apiService.editRecipe(token, recipeId, recipe, parts).enqueue(callback);
    }

    public void viewRecipe(int recipeId, Callback<Recipe> callback) {
        this.apiService.detailedRecipe(token, recipeId).enqueue(callback);
    }

    /**
     * Changes the user information. If a parameter is null, it will be left unchanged
     *
     * @param email    The new user's email
     * @param password The new user's password
     * @param callback The callback to call when the request succeeds or fails
     */
    public void editUserInfo(@Nullable String email, @Nullable String password, Callback<Status> callback) {
        this.apiService.editUser(token, email, password).enqueue(callback);
    }

    private MultipartBody.Part getPartFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
        RequestBody requestBody = RequestBody.create(MediaType.get("image/jpeg"), stream.toByteArray());
        return MultipartBody.Part.createFormData("image[]", "app_upload.jpg", requestBody);
    }

    public static void setToken(String token) {
        Connection.token = token;
    }

    public static Connection getInstance() {
        if (instance == null) {
            synchronized (Connection.class) {
                if (instance == null) {
                    instance = new Connection();
                }
            }
        }

        return instance;
    }

    public static class PicassoInstance {
        @SuppressLint("StaticFieldLeak")
        static private Picasso instance = null;

        public static Picasso getInstance(Context context, String token) {
            if (instance == null) {
                synchronized (PicassoInstance.class) {
                    if (instance == null) {
                        OkHttpClient httpClient = new OkHttpClient.Builder()
                                .addInterceptor(chain -> {
                                    Request request = chain.request().newBuilder()
                                            .header("Authorization", token)
                                            .build();
                                    return chain.proceed(request);
                                }).build();

                        instance = new Picasso.Builder(context)
                                .downloader(new OkHttp3Downloader(httpClient))
                                .build();
                    }
                }
            }

            return instance;
        }
    }
}
