package ch.hockdudu.tasty.model;

import java.io.Serializable;

public class Preparation implements Serializable {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
