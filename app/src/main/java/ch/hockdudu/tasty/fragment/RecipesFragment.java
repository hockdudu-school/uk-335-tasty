package ch.hockdudu.tasty.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.adapter.RecipesAdapter;
import ch.hockdudu.tasty.model.Recipe;


public class RecipesFragment extends Fragment {
    private static final String PARAM_TOKEN = "token";

    public final ArrayList<Recipe> recipes = new ArrayList<>();

    private String token = "";
    private OnRecipeClickedListener callback;
    private RecipesAdapter recipesAdapter;

    public RecipesFragment() {
        // Required empty public constructor
    }

    public static RecipesFragment newInstance(@NonNull String token) {
        RecipesFragment fragment = new RecipesFragment();
        Bundle args = new Bundle();

        args.putString(PARAM_TOKEN, token);

        fragment.setArguments(args);
        return fragment;
    }

    public void triggerUpdate() {
        if (recipesAdapter != null) {
            recipesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            token = getArguments().getString(PARAM_TOKEN);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recipe, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recipe_list);
        GridLayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        recipesAdapter = new RecipesAdapter(recipes, clickedRecipe -> {
            if (callback != null) {
                callback.onRecipeClicked(clickedRecipe, getTag());
            }
        }, getContext(), token);
        recyclerView.setAdapter(recipesAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRecipeClickedListener) {
            callback = (OnRecipeClickedListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement RecipesFragment.OnRecipeClickedListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public interface OnRecipeClickedListener {
        void onRecipeClicked(Recipe recipe, String tag);
    }
}
