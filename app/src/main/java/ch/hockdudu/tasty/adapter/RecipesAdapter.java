package ch.hockdudu.tasty.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.adapter.viewHolder.RecipesViewHolder;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.fragment.RecipesFragment;
import ch.hockdudu.tasty.model.Recipe;

public class RecipesAdapter extends RecyclerView.Adapter<RecipesViewHolder> {

    private List<Recipe> recipes;
    private OnRecipeClickedListener listener;
    private Picasso picasso;

    private RecipesAdapter(List<Recipe> recipes, OnRecipeClickedListener listener) {
        this.recipes = recipes;
        this.listener = listener;
    }

    public RecipesAdapter(List<Recipe> recipes, OnRecipeClickedListener listener, Context context, String token) {
        this(recipes, listener);
        this.picasso = Connection.PicassoInstance.getInstance(context, token);
    }

    @NonNull
    @Override
    public RecipesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_discover_recipe, viewGroup, false);

        return new RecipesViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecipesViewHolder recipesViewHolder, int i) {
        Recipe recipe = recipes.get(i);

        recipesViewHolder.recipeText.setText(recipe.getName());

        this.picasso.load(recipe.getImageUrl())
                .resize(500, 500)
                .centerCrop()
                .into(recipesViewHolder.recipeImage, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("RecipesAdapter", "Error when downloading image", e);
                    }
                });

        recipesViewHolder.itemView.setOnClickListener(v -> {
            if (this.listener != null) {
                listener.onRecipeClicked(recipe);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    public interface OnRecipeClickedListener {
        void onRecipeClicked(Recipe recipe);
    }
}
