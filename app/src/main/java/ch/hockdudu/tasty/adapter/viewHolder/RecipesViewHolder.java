package ch.hockdudu.tasty.adapter.viewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import ch.hockdudu.tasty.R;

public class RecipesViewHolder extends RecyclerView.ViewHolder {
    public ImageView recipeImage;
    public TextView recipeText;

    public RecipesViewHolder(View view) {
        super(view);

        recipeImage = view.findViewById(R.id.recipe_image);
        recipeText = view.findViewById(R.id.recipe_name);
    }
}
