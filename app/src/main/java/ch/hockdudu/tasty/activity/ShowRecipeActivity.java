package ch.hockdudu.tasty.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.model.Ingredient;
import ch.hockdudu.tasty.model.Preparation;
import ch.hockdudu.tasty.model.Recipe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowRecipeActivity extends BaseActivity implements View.OnClickListener {

    public final static String EXTRA_RECIPE_ID = "recipe_id";
    public final static String EXTRA_RECIPE_IS_EDITABLE = "recipe_editable";

    private int recipeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_recipe);

        //noinspection ConstantConditions
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        recipeId = intent.getIntExtra(EXTRA_RECIPE_ID, -1);
        if (recipeId == -1) {
            Log.w("ShowRecipeActivity", "Started activity without recipe id");
            finish();
            return;
        }

        View editButton = findViewById(R.id.btnEditRecipe);
        editButton.setOnClickListener(this);

        boolean isEditable = intent.getBooleanExtra(EXTRA_RECIPE_IS_EDITABLE, false);
        if (isEditable) {
            editButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
        }

        setIsLoaded(false);

        Connection.getInstance().viewRecipe(recipeId, new Callback<Recipe>() {
            @Override
            public void onResponse(@NonNull Call<Recipe> call, @NonNull Response<Recipe> response) {
                assert response.body() != null;
                onRecipeReceived(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<Recipe> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return false;
    }

    private void onRecipeReceived(Recipe recipe) {
        Connection.PicassoInstance.getInstance(ShowRecipeActivity.this, token)
                .load(recipe.getImageUrl())
                .resize(500, 400)
                .centerCrop()
                .into((ImageView) findViewById(R.id.recipeImageView));

        TextView name = findViewById(R.id.txtName);
        name.setText(recipe.getName());

        LinearLayout ingredientsListView = findViewById(R.id.ingredient_List);

        for (Ingredient ingredient : recipe.getIngredients()) {
            View ingredientFragment = LayoutInflater.from(ShowRecipeActivity.this).inflate(R.layout.fragment_ingredient, ingredientsListView, false);
            ingredientFragment.findViewById(R.id.item_ingredient_delete_btn).setVisibility(View.GONE);

            TextView ingredientName = ingredientFragment.findViewById(R.id.item_ingredient_name);
            ingredientName.setText(ingredient.getName());

            TextView ingredientQuantity = ingredientFragment.findViewById(R.id.item_ingredient_quantity);
            ingredientQuantity.setText(ingredient.getQuantity());

            ingredientsListView.addView(ingredientFragment);
        }

        LinearLayout preparationListView = findViewById(R.id.cooking_List);
        for (Preparation preparation : recipe.getPreparations()) {
            View preparationFragment = LayoutInflater.from(ShowRecipeActivity.this).inflate(R.layout.fragment_preparation, preparationListView, false);
            preparationFragment.findViewById(R.id.item_preparation_description_delete_btn).setVisibility(View.GONE);

            TextView preparationName = preparationFragment.findViewById(R.id.item_preparation_description);
            preparationName.setText(preparation.getDescription());

            preparationListView.addView(preparationFragment);
        }

        setIsLoaded(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEditRecipe:
                // TODO: Start activity for result and then update view, if necessary
                Intent intent = new Intent(this, RecipeEditActivity.class);
                intent.putExtra(RecipeEditActivity.EXTRA_RECIPE_ID, this.recipeId);
                startActivity(intent);

                break;
        }
    }

    private void setIsLoaded(boolean isLoaded) {
        findViewById(R.id.layout_wrapper).setVisibility(isLoaded ? View.VISIBLE : View.GONE);
        findViewById(R.id.progressBar).setVisibility(isLoaded ? View.GONE : View.VISIBLE);
    }
}
