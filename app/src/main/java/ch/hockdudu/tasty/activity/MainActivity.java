package ch.hockdudu.tasty.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.List;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.fragment.RecipesFragment;
import ch.hockdudu.tasty.model.Recipe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, RecipesFragment.OnRecipeClickedListener {
    private final static String FRAGMENT_DISCOVER = "discover";
    private final static String FRAGMENT_MY_RECIPES = "my_recipes";

    private int previousSelectedMenuId;
    private SearchView searchView;
    private FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isNetworkStatusAvialable(MainActivity.this)) buildDialog(MainActivity.this).show();
        else {
            Toast.makeText(MainActivity.this, "Welcome", Toast.LENGTH_SHORT).show();
            setContentView(R.layout.activity_main);
            // Adds listener to bottom navigation
            BottomNavigationView navigationView = MainActivity.this.findViewById(R.id.navigation);

            navigationView.setOnNavigationItemSelectedListener(this);

            // Sets FAB
            floatingActionButton = findViewById(R.id.fab);
            floatingActionButton.setOnClickListener(v -> startActivity(new Intent(this, RecipeEditActivity.class)));

            // Populates main fragment
            navigationView.setSelectedItemId(R.id.discover);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.app_bar_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    //Navigate between All Recipe's and Own Recipe's
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.app_bar_logout:
                preferences.edit().remove("token").apply();
                this.token = null;
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            case R.id.app_bar_profile:
                startActivity(new Intent(this, ProfileActivity.class));
                return true;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        int previousMenuId = previousSelectedMenuId;
        previousSelectedMenuId = menuItem.getItemId();

        RecipesFragment discoverFragment = (RecipesFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_DISCOVER);
        RecipesFragment myRecipesFragment = (RecipesFragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_MY_RECIPES);

        switch (menuItem.getItemId()) {
            case R.id.discover: {
                Log.v("MainActivity", "Changed to discover");

                if (token == null) {
                    Log.w("MainActivity", "Tried to load recipes without token");
                    return false;
                }

                if (previousMenuId != menuItem.getItemId() || discoverFragment == null) {

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_right_out);

                    if (myRecipesFragment != null) {
                        transaction.hide(myRecipesFragment);
                    }

                    RecipesFragment recipesFragment;
                    if (discoverFragment == null) {
                        recipesFragment = RecipesFragment.newInstance(token);
                        transaction.add(R.id.main_frame, recipesFragment, FRAGMENT_DISCOVER);
                    } else {
                        recipesFragment = discoverFragment;
                        transaction.show(discoverFragment);
                    }

                    transaction.commit();

                    // TODO: Add loading spinner
                    Connection.getInstance().featuredRecipes(new Callback<List<Recipe>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Recipe>> call, @NonNull Response<List<Recipe>> response) {
                            assert response.body() != null;

                            recipesFragment.recipes.clear();
                            recipesFragment.recipes.addAll(response.body());
                            recipesFragment.triggerUpdate();
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Recipe>> call, @NonNull Throwable t) {
                            Log.e("MainActivity", "Error when downloading featured recipes", t);
                        }
                    });
                }

                floatingActionButton.hide();

                return true;
            }
            case R.id.my_recipes: {
                Log.v("MainActivity", "Changed to my recipes");

                if (token == null) {
                    Log.w("MainActivity", "Tried to load recipes without token");
                    return false;
                }

                if (previousMenuId != menuItem.getItemId() || myRecipesFragment == null) {

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_left_out);

                    if (discoverFragment != null) {
                        transaction.hide(discoverFragment);
                    }

                    RecipesFragment recipesFragment;
                    if (myRecipesFragment == null) {
                        recipesFragment = RecipesFragment.newInstance(token);
                        transaction.add(R.id.main_frame, recipesFragment, FRAGMENT_MY_RECIPES);
                    } else {
                        recipesFragment = myRecipesFragment;
                        transaction.show(recipesFragment);
                    }

                    transaction.commit();


                    // TODO: Add loading spinner
                    Connection.getInstance().myRecipes(new Callback<List<Recipe>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<Recipe>> call, @NonNull Response<List<Recipe>> response) {
                            assert response.body() != null;

                            recipesFragment.recipes.clear();
                            recipesFragment.recipes.addAll(response.body());
                            recipesFragment.triggerUpdate();
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<Recipe>> call, @NonNull Throwable t) {
                            Log.e("MainActivity", "Error when downloading own recipes (my recipes)", t);
                        }
                    });
                }

                floatingActionButton.show();

                return true;
            }
        }

        return false;
    }

    //Click on one of the Recipe's in the Main list
    @Override
    public void onRecipeClicked(Recipe recipe, String tag) {
        Intent intent = new Intent(this, ShowRecipeActivity.class);
        intent.putExtra(ShowRecipeActivity.EXTRA_RECIPE_ID, recipe.getId());

        if (FRAGMENT_MY_RECIPES.equals(tag)) {
            intent.putExtra(ShowRecipeActivity.EXTRA_RECIPE_IS_EDITABLE, true);
        }

        startActivity(intent);
    }

    //Check if there is an Internet connection
    public static boolean isNetworkStatusAvialable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfos = connectivityManager.getActiveNetworkInfo();

        if (netInfos == null) {
            return false;
        } else if (netInfos.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mobile != null && mobile.isConnectedOrConnecting() || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        AlertDialog.Builder ok = builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        return builder;
    }
}
