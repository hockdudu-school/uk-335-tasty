package ch.hockdudu.tasty.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.connection.model.Token;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity implements View.OnClickListener, TextWatcher {
    private static final Pattern PASSWORD_PATTERN =

            //Regex for Validation
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");

    //Define the parameter's
    EditText etName, etPassword, etRePassword, etEmail;

    TextView loginLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //get the values from the Fields
        etName = findViewById(R.id.etRegUsername);
        etPassword = findViewById(R.id.etRegPassword);
        etRePassword = findViewById(R.id.etRegRePassword);
        etEmail = findViewById(R.id.etRegEmail);

        loginLink = findViewById(R.id.txtLogin);
        loginLink.setOnClickListener(this);
    }

    //Validation method for Email Field
    private boolean validateEmail() {
        String emailInput = etEmail.getText().toString();

        //Check if the Email Field is Empty
        if (emailInput.isEmpty()) {
            etEmail.setError("Field can't be Empty");
            return false;
            //Compare the given Email to the Regex
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            etEmail.setError("Please enter a valid email address");
            return false;
            //Set an Error if the given Email don't correspond the given Requirements
        } else {
            etEmail.setError(null);
            return true;
        }
    }

    //Validation method for Email Field
    private boolean validatePassword() {
        String passwordInput = etPassword.getText().toString();
        String rePasswordInput = etRePassword.getText().toString();

        //Check if the Password Field is Empty
        if (passwordInput.isEmpty()) {
            etPassword.setError("Field can't be Empty");
            return false;

            //Compare the given Password to the Regex
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            etPassword.setError("Password must have at least 1 digit, 1 big letter, 1 small letter, 1 special character, no white space");
            return false;

            //Set an Error if the given Re-Password don't correspond the Password
        } else if (!passwordInput.matches(rePasswordInput)) {
            etRePassword.setError("Passwords do not match!");
            return false;
        } else {

            //Set an Error if the given Password don't correspond the given Requirements
            etPassword.setError(null);
            return true;
        }
    }

    //Validation method for Username Field
    private boolean validateUsername() {
        String usernameInput = etName.getText().toString();

        //Set an Error if the given Username doesn't exists
        if (usernameInput.isEmpty()) {
            etName.setError("Field can't be empty");
            return false;
            //Set an Error if the given Username doesn't exists
        } else if (usernameInput.length() > 15) {
            etName.setError("Username too long");
            return false;
        } else {
            etName.setError(null);
            return true;
        }
    }

    //Confirm the Registering Fields
    public void confirmRegInput(View v) {
        if (!validateEmail() | !validateUsername() | !validatePassword()) {
            return;
        }

        //The Output Before the creation of the API
        String input = "Email: " + etEmail.getText().toString();
        input += "\n";
        input += "Username: " + etName.getText().toString();
        input += "\n";
        input += "Password: " + etPassword.getText().toString();

        Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
    }

    //Check if the given user exists in the Server, if not then create a new account
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
                Connection.getInstance().signup(etName.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString(), new Callback<Token>() {
                    @Override
                    public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                        assert response.body() != null;
                        setToken(response.body().token);

                        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                        // TODO: Handle error
                    }
                });
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    //calling the validation method for the input's
    @SuppressLint("ResourceType")
    @Override
    public void afterTextChanged(Editable s) {
        confirmRegInput(findViewById(R.layout.activity_register));
    }
}
