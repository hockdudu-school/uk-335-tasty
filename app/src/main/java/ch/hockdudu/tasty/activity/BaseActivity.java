package ch.hockdudu.tasty.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import ch.hockdudu.tasty.connection.Connection;

public abstract class BaseActivity extends AppCompatActivity {

    protected SharedPreferences preferences;
    @Nullable
    protected String token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        String token = preferences.getString("token", null);
        if (null != token) {
            setToken(token);
        }
    }


    protected void setToken(@NonNull String token) {
        this.token = token;

        if (!token.equals(preferences.getString("token", null))) {
            preferences.edit()
                    .putString("token", token)
                    .apply();
        }

        Connection.setToken(token);

    }
}
