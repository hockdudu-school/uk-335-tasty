package ch.hockdudu.tasty.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.connection.model.Token;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener, TextWatcher {
    TextView registerLink;
    EditText etPassword, etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //get the values from the Fields
        etName = findViewById(R.id.etLoginUsername);
        etPassword = findViewById(R.id.etLoginPassword);

        registerLink = findViewById(R.id.txtRegister);
        registerLink.setOnClickListener(this);

        Button loginButton = findViewById(R.id.btnLogin);
        loginButton.setOnClickListener(this);
    }

    //Check if the Username exist and the Field is not Empty
    private boolean validateUsername() {
        String nameInput = etName.getText().toString();
        if (nameInput.isEmpty()) {
            etName.setError("Field can't be Empty");
            return false;
        } else {
            etName.setError(null);
            return true;
        }
    }

    //Check if the  Password exist and the Field is not Empty
    private boolean validatePassword() {
        String passwordInput = etPassword.getText().toString();
        if (passwordInput.isEmpty()) {
            etPassword.setError("Field can't be Empty");
            return false;
        } else {
            etPassword.setError(null);
            return true;
        }
    }

    //Check if the both Username and Password exist
    public void confirmLogInput(View v) {
        validateUsername();
        validatePassword();
    }

    //Check if the given user exists in the Server, if Yes then Login
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtRegister:
                startActivity(new Intent(this, RegisterActivity.class));
                finish();
                break;
            case R.id.btnLogin:
                if (!validateUsername() || !validatePassword()) {
                    return;
                }

                Connection.getInstance().login(etName.getText().toString(), etPassword.getText().toString(), new Callback<Token>() {
                    @Override
                    public void onResponse(@NonNull Call<Token> call, @NonNull Response<Token> response) {
                        assert response.body() != null;
                        setToken(response.body().token);

                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    public void onFailure(@NonNull Call<Token> call, @NonNull Throwable t) {
                        // TODO: Handle error
                    }
                });
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    //calling the validation method for the input's
    @SuppressLint("ResourceType")
    @Override
    public void afterTextChanged(Editable s) {
        confirmLogInput(findViewById(R.layout.activity_login));
    }
}