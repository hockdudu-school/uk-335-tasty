package ch.hockdudu.tasty.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.model.Ingredient;
import ch.hockdudu.tasty.model.Preparation;
import ch.hockdudu.tasty.model.Recipe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeEditActivity extends BaseActivity {

    private static final int GALLERY_REQUEST_CODE = 1;
    private static final int GALLERY_RESULT_CODE = 1;
    ImageView recipeImageView;

    public final static String EXTRA_RECIPE_ID = "recipe_id";

    private boolean isEditMode = false;
    private int editModeRecipeId;

    private String cachedImageUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_and_edit_recipe);

        addListenerOnImageAddButton();
        addListenerOnEditIngredientButton();
        addListenerOnEditPreparationButton();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        int recipeId = intent.getIntExtra(EXTRA_RECIPE_ID, -1);
        if (recipeId == -1) {
            setIsLoaded(true);
            return;
        }

        isEditMode = true;
        editModeRecipeId = recipeId;
        setIsLoaded(false);

        Connection.getInstance().viewRecipe(recipeId, new Callback<Recipe>() {
            @Override
            public void onResponse(@NonNull Call<Recipe> call, @NonNull Response<Recipe> response) {
                assert response.body() != null;

                onEditRecipeReceived(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<Recipe> call, @NonNull Throwable t) {

            }
        });
    }

    private void onEditRecipeReceived(Recipe recipe) {
        Connection.PicassoInstance.getInstance(RecipeEditActivity.this, token)
                .load(recipe.getImageUrl())
                .resize(500, 400)
                .centerCrop()
                .into((ImageView) findViewById(R.id.recipe_image));

        cachedImageUrl = recipe.getImageUrl();

        TextView name = findViewById(R.id.name);
        name.setText(recipe.getName());

        TextView description = findViewById(R.id.description);
        description.setText(recipe.getDescription());

        // TODO: Add cooking time

        LinearLayout ingredientsListView = findViewById(R.id.ingredient_list);
        for (Ingredient ingredient : recipe.getIngredients()) {
            View ingredientFragment = LayoutInflater.from(RecipeEditActivity.this).inflate(R.layout.fragment_ingredient, ingredientsListView, false);
            TextView ingredientName = ingredientFragment.findViewById(R.id.item_ingredient_name);
            ingredientName.setText(ingredient.getName());
            TextView ingredientQuantity = ingredientFragment.findViewById(R.id.item_ingredient_quantity);
            ingredientQuantity.setText(ingredient.getQuantity());
            ingredientsListView.addView(ingredientFragment);
        }

        LinearLayout preparationListView = findViewById(R.id.preparation_list);
        for (Preparation preparation : recipe.getPreparations()) {
            View preparationFragment = LayoutInflater.from(RecipeEditActivity.this).inflate(R.layout.fragment_preparation, preparationListView, false);
            TextView preparationName = preparationFragment.findViewById(R.id.item_preparation_description);
            preparationName.setText(preparation.getDescription());
            preparationListView.addView(preparationFragment);
        }

        setIsLoaded(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.save_btn:
                onSave();
                return true;
        }
        return false;
    }

    private void onSave() {
        Recipe recipe = new Recipe();

        EditText name = findViewById(R.id.name);
        recipe.setName(name.getText().toString());

        EditText description = findViewById(R.id.description);
        recipe.setDescription(description.getText().toString());

        EditText preparationTime = findViewById(R.id.preparation_time);
        try {
            recipe.setPreparationTime(Integer.parseInt(preparationTime.getText().toString()));
        } catch (Exception ignored) {

        }

        LinearLayout preparationListView = findViewById(R.id.preparation_list);
        int numberOfFragmentPreparation = preparationListView.getChildCount();
        for (int i = 0; i < numberOfFragmentPreparation; i++) {
            View preparationFragmentView = preparationListView.getChildAt(i);
            TextView preparationDescription = preparationFragmentView.findViewById(R.id.item_preparation_description);
            Preparation preparation = new Preparation();
            preparation.setDescription(preparationDescription.getText().toString());
            recipe.getPreparations().add(preparation);
        }

        LinearLayout ingredientsListView = findViewById(R.id.ingredient_list);
        int numberOfFragmentIngredientsView = ingredientsListView.getChildCount();
        for (int i = 0; i < numberOfFragmentIngredientsView; i++) {
            View ingredientsFragmentView = ingredientsListView.getChildAt(i);
            TextView ingredientsName = ingredientsFragmentView.findViewById(R.id.item_ingredient_name);
            TextView ingredientQuantity = ingredientsFragmentView.findViewById(R.id.item_ingredient_quantity);
            Ingredient ingredient = new Ingredient();
            ingredient.setName(ingredientsName.getText().toString());
            ingredient.setQuantity(ingredientQuantity.getText().toString());
            recipe.getIngredients().add(ingredient);
        }

        ImageView image = findViewById(R.id.recipe_image);
        BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
        if (drawable != null) {
            Bitmap bitmap = drawable.getBitmap();
            recipe.setRecipeImage(bitmap);
        }


        if (isEditMode) {
            if (cachedImageUrl != null) {
                recipe.setImageUrl(cachedImageUrl);
            }

            Toast.makeText(this, "Updating", Toast.LENGTH_SHORT).show();
            Connection.getInstance().editRecipe(recipe, editModeRecipeId, new Callback<Recipe>() {
                @Override
                public void onResponse(@NonNull Call<Recipe> call, @NonNull Response<Recipe> response) {
                    Toast.makeText(RecipeEditActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onFailure(@NonNull Call<Recipe> call, @NonNull Throwable t) {
                    Toast.makeText(RecipeEditActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    Log.e("RecipeEditActivity", "Error when updating recipe", t);
                }
            });
        } else {
            Toast.makeText(this, "Saving", Toast.LENGTH_SHORT).show();
            Connection.getInstance().createRecipe(recipe, new Callback<Recipe>() {
                @Override
                public void onResponse(@NonNull Call<Recipe> call, @NonNull Response<Recipe> response) {
                    Toast.makeText(RecipeEditActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                    finish();
                }

                @Override
                public void onFailure(@NonNull Call<Recipe> call, @NonNull Throwable t) {
                    Toast.makeText(RecipeEditActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    Log.e("RecipeEditActivity", "Error when saving recipe", t);
                }
            });
        }
    }

    private void addListenerOnImageAddButton() {
        recipeImageView = findViewById(R.id.recipe_image);
        ImageButton button = findViewById(R.id.btn_change_image);
        button.setOnClickListener(v -> pickFromGallery());
    }

    private void pickFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

        startActivityForResult(intent, GALLERY_REQUEST_CODE);
        onActivityResult(GALLERY_REQUEST_CODE, GALLERY_RESULT_CODE, intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST_CODE:
                    //data.getData returns the content URI for the selected Image
                    Uri selectedImage = data.getData();
                    recipeImageView.setImageURI(selectedImage);
                    break;
            }
    }

    private void addListenerOnEditIngredientButton() {
        ImageButton editIngredientButton = findViewById(R.id.ingredient_edit_btn);
        editIngredientButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(RecipeEditActivity.this);
            final View mView = getLayoutInflater().inflate(R.layout.add_ingredient, null);
            builder.setView(mView);
            builder.setPositiveButton("Add", (dialog, which) -> {
                final LinearLayout listView = findViewById(R.id.ingredient_list);
                EditText nameEditText = mView.findViewById(R.id.ingredient_name);
                final String name = nameEditText.getText().toString();
                EditText quantityEditText = mView.findViewById(R.id.ingredient_quantity);
                final String quantity = quantityEditText.getText().toString();
                final View ingredientFragment = LayoutInflater.from(RecipeEditActivity.this).inflate(R.layout.fragment_ingredient, listView, false);
                listView.addView(ingredientFragment);
                final TextView ingredientName = ingredientFragment.findViewById(R.id.item_ingredient_name);
                final TextView ingredientQuantity = ingredientFragment.findViewById(R.id.item_ingredient_quantity);
                ImageButton ingredientItemDeleteBtn = ingredientFragment.findViewById(R.id.item_ingredient_delete_btn);
                ingredientItemDeleteBtn.setOnClickListener(v1 -> {
                    listView.removeView(ingredientFragment);
                    Snackbar deleteSnackbar = Snackbar.make(listView, "Ingredient is deleted", Snackbar.LENGTH_LONG)
                            .setAction("UNDO", view -> {
                                listView.addView(ingredientFragment);
                                Snackbar restoreSnackbar = Snackbar.make(listView, "Ingredient is restored!", Snackbar.LENGTH_SHORT);
                                restoreSnackbar.show();
                            });

                    deleteSnackbar.show();
                });
                ingredientName.setText(name);
                ingredientQuantity.setText(quantity);

            });

            builder.setNegativeButton("Cancel", (dialog, which) -> {

            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

    }

    private void addListenerOnEditPreparationButton() {

        ImageButton editIngredientButton = findViewById(R.id.preparation_edit_btn);
        editIngredientButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(RecipeEditActivity.this);
            final View mView = getLayoutInflater().inflate(R.layout.add_preparation, null);
            builder.setView(mView);
            builder.setPositiveButton("Add", (dialog, which) -> {
                final LinearLayout listView = findViewById(R.id.preparation_list);
                EditText descriptionEditText = mView.findViewById(R.id.preparation_description);
                String description = descriptionEditText.getText().toString();
                final View preparationFragment = LayoutInflater.from(RecipeEditActivity.this).inflate(R.layout.fragment_preparation, listView, false);
                listView.addView(preparationFragment);
                TextView textView = preparationFragment.findViewById(R.id.item_preparation_description);
                ImageButton descrptionItemDeleteBtn = preparationFragment.findViewById(R.id.item_preparation_description_delete_btn);
                descrptionItemDeleteBtn.setOnClickListener(v1 -> {
                    listView.removeView(preparationFragment);
                    Snackbar deleteSnackbar = Snackbar.make(listView, "Preparation is deleted", Snackbar.LENGTH_LONG)
                            .setAction("UNDO", view -> {
                                listView.addView(preparationFragment);
                                Snackbar restoreSnackbar = Snackbar.make(listView, "Preparation is restored!", Snackbar.LENGTH_SHORT);
                                restoreSnackbar.show();
                            });

                    deleteSnackbar.show();
                });
                textView.setText(description);
            });

            builder.setNegativeButton("Cancel", (dialog, which) -> {

            });
            AlertDialog dialog = builder.create();
            dialog.show();
        });

    }

    private void setIsLoaded(boolean isLoaded) {
        findViewById(R.id.layout_wrapper).setVisibility(isLoaded ? View.VISIBLE : View.GONE);
        findViewById(R.id.progressBar).setVisibility(isLoaded ? View.GONE : View.VISIBLE);
    }
}
