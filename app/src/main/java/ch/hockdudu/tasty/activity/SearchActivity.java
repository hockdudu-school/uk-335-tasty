package ch.hockdudu.tasty.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import ch.hockdudu.tasty.R;
import ch.hockdudu.tasty.connection.Connection;
import ch.hockdudu.tasty.fragment.RecipesFragment;
import ch.hockdudu.tasty.model.Recipe;
import ch.hockdudu.tasty.provider.SuggestionProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity implements RecipesFragment.OnRecipeClickedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (!Intent.ACTION_SEARCH.equals(intent.getAction())) {
            return;
        }

        String query = intent.getStringExtra(SearchManager.QUERY);

        ActionBar actionBar = Objects.requireNonNull(getSupportActionBar());
        actionBar.setTitle(query);
        actionBar.setDisplayHomeAsUpEnabled(true);

        SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this, SuggestionProvider.AUTHORITY, SuggestionProvider.MODE);
        suggestions.saveRecentQuery(query, null);

        doQuery(query);
    }

    private void doQuery(String query) {
        Connection.getInstance().searchRecipes(query, new Callback<List<Recipe>>() {
            @Override
            public void onResponse(@NonNull Call<List<Recipe>> call, @NonNull Response<List<Recipe>> response) {
                assert response.body() != null;
                displaySearchResult(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<Recipe>> call, @NonNull Throwable t) {

            }
        });
    }

    private void displaySearchResult(List<Recipe> recipeList) {
        ProgressBar progressBar = findViewById(R.id.search_spinning_wheel);
        progressBar.setVisibility(View.GONE);

        if (token != null) {
            RecipesFragment recipesFragment = RecipesFragment.newInstance(token);
            recipesFragment.recipes.addAll(recipeList);
            recipesFragment.triggerUpdate();
            getSupportFragmentManager().beginTransaction().add(R.id.search_result_frame, recipesFragment, "searchResultFragment").commit();
        } else {
            Log.w("SearchActivity", "Tried to display search results with null token");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //noinspection SwitchStatementWithTooFewBranches
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onRecipeClicked(Recipe recipe, String tag) {
        Intent intent = new Intent(this, ShowRecipeActivity.class);
        intent.putExtra(ShowRecipeActivity.EXTRA_RECIPE_ID, recipe.getId());
        startActivity(intent);
    }
}
