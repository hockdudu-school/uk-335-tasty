package ch.hockdudu.tasty.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;
import java.util.regex.Pattern;

import ch.hockdudu.tasty.R;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {

    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");
    EditText etNewPassword, etReNewPassword, etEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);

        etEmail = findViewById(R.id.etEmail);
        etNewPassword = findViewById(R.id.etNewPassword);
        etReNewPassword = findViewById(R.id.etRe_NewPassword);

        Objects.requireNonNull(this.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
    }

    private boolean validateEmail() {
        String emailInput = etEmail.getText().toString();

        if (emailInput.isEmpty()) {
            etEmail.setError("Field can't be Empty");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            etEmail.setError("Please enter a valid email address");
            return false;
        } else {
            etEmail.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String newPasswordInput = etNewPassword.getText().toString();
        String reNewPasswordInput = etReNewPassword.getText().toString();

        if (newPasswordInput.isEmpty()) {
            etNewPassword.setError("Field can't be Empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(newPasswordInput).matches()) {
            etNewPassword.setError("Password must have at least 1 digit, 1 big letter, 1 small letter, 1 special character, no white space");
            return false;
        } else if (!newPasswordInput.matches(reNewPasswordInput)) {
            etReNewPassword.setError("Passwords do not match!");
            return false;
        } else {
            etNewPassword.setError(null);
            return true;
        }
    }


    public void confirmEditTInput(View v) {
        if (!validateEmail() | !validatePassword()) {
            return;
        }

        String input = "Email: " + etEmail.getText().toString();
        input += "\n";
        input += "Password: " + etNewPassword.getText().toString();

        Toast.makeText(this, input, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        return true;
    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_btn:
                confirmEditTInput(findViewById(R.layout.activity_edit_user_profile));
                return true;
        }
        return false;
    }


    public void dialogEvent(View v) {
        TextView txtDelete = findViewById(R.id.txtDelete);
        txtDelete.setOnClickListener(v1 -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
            builder.setMessage("Are you sure want to delete your account?").setCancelable(false)
                    .setPositiveButton(Html.fromHtml("<font color='#FF7F27'>Yes</font>"), (dialog, which) -> {

                    })
                    .setNegativeButton(Html.fromHtml("<font color='#FF7F27'>No</font>"), (dialog, which) -> dialog.cancel());

            AlertDialog alert = builder.create();
            alert.setTitle("Delete Account");
            alert.show();
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @SuppressLint("ResourceType")
    @Override
    public void afterTextChanged(Editable s) {
        confirmEditTInput(findViewById(R.layout.activity_edit_user_profile));
    }
}
